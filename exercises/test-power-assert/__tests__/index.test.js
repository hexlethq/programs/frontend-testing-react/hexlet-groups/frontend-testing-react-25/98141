const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, 2, 3, [4, 5, [6, 7]]];
const table = {
  test1: {
    actual: array,
    expected: [1, 2, 3, 4, 5, [6, 7]],
    arg: undefined,
  },
  test2: {
    actual: array,
    expected: [1, 2, 3, 4, 5, [6, 7]],
    arg: 1,
  },
  test3: {
    actual: array,
    expected: [1, 2, 3, 4, 5, 6, 7],
    arg: 2,
  },
  test4: {
    actual: array,
    expected: [1, 2, 3, 4, 5, 6, 7],
    arg: 10,
  },
  test5: {
    actual: array,
    expected: [1, 2, 3, [4, 5, [6, 7]]],
    arg: 0,
  },
  test6: {
    actual: array,
    expected: [1, 2, 3, [4, 5, [6, 7]]],
    arg: -1,
  },
  test7: {
    actual: array,
    expected: [1, 2, 3, [4, 5, [6, 7]]],
    arg: null,
  },
  test8: {
    actual: array,
    expected: [1, 2, 3, [4, 5, [6, 7]]],
    arg: {},
  },
};

Object.keys(table).forEach((testCase) => {
  assert.deepStrictEqual(
    flattenDepth(table[testCase].actual, table[testCase].arg),
    table[testCase].expected,
  );
});
// END

// ПЕРЕД ТЕМ КАК НАЧАТЬ РЕШАТЬ ЗАДАНИЕ
// УДАЛИТЕ СЛЕДУЮЩУЮ СТРОКУ
/* assert.ok(done); */
