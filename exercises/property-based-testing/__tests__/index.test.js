const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('sort', () => {
  it('should sort integer correct and return new array with similar lenght', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer()),
        (array) => {
          const sortedArray = sort(array);

          expect(sortedArray).toBeSorted();
          expect(sortedArray).not.toBe(array);
          expect(sortedArray).toHaveLength(array.length);
        },
      ),
    );
  });

  it('should sort numeric string correct and return new array with similar lenght', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer().map(String)),
        (array) => {
          const sortedArray = sort(array);

          expect(sortedArray).toBeSorted({ coerce: true });
          expect(sortedArray).not.toBe(array);
          expect(sortedArray).toHaveLength(array.length);
        },
      ),
    );
  });

  it('should sort numeric and string correct and return new array with similar lenght', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer().map((item) => (item % 2 === 0 ? item.toString() : item))),
        (array) => {
          const sortedArray = sort(array);

          expect(sortedArray).toBeSorted({ coerce: true });
          expect(sortedArray).not.toBe(array);
          expect(sortedArray).toHaveLength(array.length);
        },
      ),
    );
  });

  it('should not sort and throw error if not array', () => {
    fc.assert(
      fc.property(
        fc.oneof(fc.integer(), fc.string(), fc.boolean()),
        (random) => {
          expect(() => sort(random)).toThrow();
        },
      ),
    );
  });
});
// END
