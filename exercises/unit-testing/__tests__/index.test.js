describe('Object.assign', () => {
  it('main', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);
    const mockResult = {
      k: 'v',
      b: 'b',
      a: 'a',
    };
    expect.hasAssertions();
    // BEGIN
    expect(result).toEqual(mockResult);
    expect(target).toBe(result);
    // END
  });

  it('should abort to copy and throw error', () => {
    const src = { testField: 'v', b: 'b' };
    const target = Object.defineProperty({}, 'testField', {
      value: 'test',
      writable: false,
    });
    // BEGIN
    expect(() => Object.assign(target, src)).toThrow();
    // END
  });

  it('should not copied non-enumerated and inhired properties', () => {
    const src = Object.create({ testInheritedField: 'test' }, {
      testNotEnumerableField: { value: 1, enumerable: false },
      testEnumerableField: { value: 2, enumerable: true },
    });
    const mockResult = {
      testEnumerableField: 2,
    };
    // BEGIN
    /* eslint-disable prefer-object-spread */
    expect(Object.assign({}, src)).toStrictEqual(mockResult);
    // END
  });
});
