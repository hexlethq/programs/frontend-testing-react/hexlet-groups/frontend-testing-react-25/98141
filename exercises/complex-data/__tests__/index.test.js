const faker = require('faker');

// BEGIN
describe('faker.helpers.createTransaction', () => {
  it('should return object with "amount", "date", "business", "name", "type", "account" fields', () => {
    const mockTransaction = faker.helpers.createTransaction();

    expect(mockTransaction).toEqual(
      expect.objectContaining({
        amount: expect.any(String),
        date: expect.any(Date),
        business: expect.any(String),
        name: expect.any(String),
        type: expect.any(String),
        account: expect.any(String),
      }),
    );
  });

  it('should create unique transactions', () => {
    const mockListOfTransactions = [...Array(100).keys()]
      .map(() => faker.helpers.createTransaction());
    const expectedObj = faker.helpers.createTransaction();

    expect(mockListOfTransactions).toMatchObject(
      expect.not.arrayContaining([
        expect.objectContaining(expectedObj),
      ]),
    );
  });
});
// END
