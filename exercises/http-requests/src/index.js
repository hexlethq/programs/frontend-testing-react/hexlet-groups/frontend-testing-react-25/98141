const axios = require('axios');

// BEGIN
axios.defaults.baseURL = 'https://httpbin.org';
const get = async (url) => {
  try {
    const { data } = await axios.get(url);

    return data;
  } catch (error) {
    throw Error(error);
  }
};

const post = async (url, body) => {
  try {
    const { data } = await axios.post(url, body);

    return data;
  } catch (error) {
    throw Error(error);
  }
};
// END

module.exports = { get, post };
