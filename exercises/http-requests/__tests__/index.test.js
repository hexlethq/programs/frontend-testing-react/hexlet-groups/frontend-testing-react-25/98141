const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index');

axios.defaults.adapter = require('axios/lib/adapters/http');

const user = {
  name: 'name',
  id: 123,
  role: 'role',
};
const tableError = [
  [404],
  [403],
  [401],
  [500],
  [502],
  [503],
];
// BEGIN
describe('CRUD', () => {
  beforeEach(() => {
    nock.disableNetConnect();
  });

  afterEach(() => {
    nock.enableNetConnect();
    nock.cleanAll();
  });

  describe('get', () => {
    it('should get data by url', async () => {
      const scope = nock('https://httpbin.org')
        .get('/json')
        .reply(200, user);

      const data = await get('/json');

      expect(scope.isDone()).toBeTruthy();
      expect(data).toEqual(user);
    });

    test.each(tableError)('should %s error by get', async (status) => {
      nock('https://httpbin.org')
        .get('/json')
        .reply(status);

      await expect(async () => get('/json')).rejects.toThrow();
    });
  });

  describe('post', () => {
    it('should create data by url', async () => {
      const scope = nock('https://httpbin.org')
        .post('/post', user)
        .reply(201, user);

      const data = await post('/post', user);

      expect(scope.isDone()).toBeTruthy();
      expect(data).toEqual(user);
    });

    test.each(tableError)('should %s error by post', async (status) => {
      nock('https://httpbin.org')
        .post('/json', user)
        .reply(status);

      await expect(async () => post('/json', user)).rejects.toThrow();
    });
  });
});
// END
