const fs = require('fs');

// BEGIN
const isFileExists = (file) => {
  const flag = true;

  fs.accessSync(file, fs.constants.F_OK);

  return flag;
};

const getFileData = (file) => {
  const data = fs.readFileSync(file, 'utf-8');

  return JSON.parse(data);
};

const upCountFromString = (str) => (findIndex, separator = '.') => str.split(separator)
  .map((item, i) => (i === findIndex ? String(Number(item) + 1) : item))
  .join(separator);

const changeFileVersion = (file, version) => {
  fs.writeFileSync(file, JSON.stringify(version));
};

const upVersion = (path, versionUpdateType = 'patch') => {
  let versionUpdateTypeNormolized = versionUpdateType;

  if (versionUpdateType === '') {
    versionUpdateTypeNormolized = 'patch';
  }

  if (!isFileExists(path)) {
    throw Error('File is not valid');
  }

  const fileData = getFileData(path);
  const fileVersion = fileData.version;
  const upVersionByIndex = upCountFromString(fileVersion);
  const getNewVersion = (version) => ({ ...fileData, version });

  switch (versionUpdateTypeNormolized) {
    case 'patch':
      changeFileVersion(path, getNewVersion(upVersionByIndex(2)));
      break;
    case 'minor':
      changeFileVersion(path, getNewVersion(upVersionByIndex(1)));
      break;
    case 'major':
      changeFileVersion(path, getNewVersion(upVersionByIndex(0)));
      break;
    default:
      throw Error('Version type is invalid');
  }
};
// END

module.exports = { upVersion };
