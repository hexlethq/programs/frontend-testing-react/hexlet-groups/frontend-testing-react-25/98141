const fs = require('fs');
const path = require('path');

const { upVersion } = require('../src/index');

// BEGIN
const getFixturePath = (filename) => path.join('__fixtures__', filename);
const getPackageVersion = (filePath) => {
  const data = fs.readFileSync(filePath, 'utf-8');

  return JSON.parse(data);
};
const fixturePath = getFixturePath('package.json');
const table = [
  {
    versionLevel: undefined,
    filePath: fixturePath,
    expected: {
      version: '1.3.3',
    },
  },
  {
    versionLevel: '',
    filePath: fixturePath,
    expected: {
      version: '1.3.3',
    },
  },
  {
    versionLevel: 'patch',
    filePath: fixturePath,
    expected: {
      version: '1.3.3',
    },
  },
  {
    versionLevel: 'minor',
    filePath: fixturePath,
    expected: {
      version: '1.4.2',
    },
  },
  {
    versionLevel: 'major',
    filePath: fixturePath,
    expected: {
      version: '2.3.2',
    },
  },
];

const tableError = [
  ['invalid.path', ''],
  [fixturePath, 'invalid version level'],
];

describe('upVersion', () => {
  afterEach(() => {
    try {
      fs.writeFileSync(fixturePath, '{"version":"1.3.2"}');
    } catch (error) {
      console.log(error);
    }
  });

  test.each(table)('should  expect %o', ({ versionLevel, filePath, expected }) => {
    upVersion(filePath, versionLevel);

    expect(getPackageVersion(filePath)).toEqual(expected);
  });

  test.each(tableError)('should  trow error if invalid path or level', ({ filePath, versionLevel }) => {
    expect(() => upVersion(filePath, versionLevel)).toThrow();
  });
});

// END
